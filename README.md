# IOC for TS2 TPG vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_tpg300_500](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tpg300_500.git)


## Controlled devices

*   TS2-020Row:Vac-VEG-10001
    *   TS2-010CRM:Vac-VGC-10000
    *   TS2-010CRM:Vac-VGC-20000
*   TS2-020Row:Vac-VEG-10002
    *   TS2-010CRM:Vac-VGC-30000
    *   TS2-010CRM:Vac-VGC-40000
