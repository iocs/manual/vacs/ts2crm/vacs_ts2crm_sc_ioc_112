#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_tpg300_500
#
require vac_ctrl_tpg300_500


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_tpg300_500_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-020Row:Vac-VEG-10001
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_ctrl_tpg300_500_moxa.iocsh, "DEVICENAME = TS2-020Row:Vac-VEG-10001, IPADDR = moxa-vac-ts2.tn.esss.lu.se, PORT = 4002")

#
# Device: TS2-020Row:Vac-VEG-10002
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_ctrl_tpg300_500_moxa.iocsh, "DEVICENAME = TS2-020Row:Vac-VEG-10002, IPADDR = moxa-vac-ts2.tn.esss.lu.se, PORT = 4003")

#
# Device: TS2-010CRM:Vac-VGC-10000
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = TS2-010CRM:Vac-VGC-10000, CHANNEL = A1, CONTROLLERNAME = TS2-020Row:Vac-VEG-10001")

#
# Device: TS2-010CRM:Vac-VGC-20000
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = TS2-010CRM:Vac-VGC-20000, CHANNEL = B1, CONTROLLERNAME = TS2-020Row:Vac-VEG-10001")

#
# Device: TS2-010CRM:Vac-VGC-30000
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = TS2-010CRM:Vac-VGC-30000, CHANNEL = A1, CONTROLLERNAME = TS2-020Row:Vac-VEG-10002")

#
# Device: TS2-010CRM:Vac-VGC-40000
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = TS2-010CRM:Vac-VGC-40000, CHANNEL = B1, CONTROLLERNAME = TS2-020Row:Vac-VEG-10002")

#
# Disable memory locking
#
var dbThreadRealtimeLock 0
